@auth.requires_membership('admin')
              
def create():
   import datetime
   db.rent.seller.default = auth.user.id
   form = crud.create(db.rent)  
   if form.process().accepted:
       session.flash = 'new blog rent created'
       redirect(URL("rent","view", args=form.vars.id))
   return dict(form=form)
   
@auth.requires(auth.user_id==db.rent(request.args(0, cast=int)).seller
                or auth.has_membership(role="admin")
              )			  
def edit():
   rent = db.rent(request.args(0, cast=int)) or redirect(URL('default','index'))
   form = crud.update("rent", rent.id, next=URL("view", args=rent.id ), 
                      message="rent updated")
   return dict(rent=rent, form=form)



def view():
   rent = db.rent(request.args(0, cast=int)) or redirect(URL('default','index'))
   rent_author = db.auth_user(rent.seller)   
   if auth.is_logged_in():   
       db.comment1.rent_id.default = rent.id
       db.comment1.writter.default = auth.user.id
       form = crud.create(db.comment1)
       if form.process().accepted:
            response.flash = 'your comment is posted'
   else:
       form = None         
   comment1 = db(db.comment1.rent_id == rent.id).select()
   for comment in comment1:
      author = db(db.auth_user.id == comment.writter).select()[0]
      comment.author = author.first_name + " " + author.last_name
      name = db(db.auth_user.id == comment.writter).select()[0]
      comment.name = name.username
   return dict(rent=rent, comment1=comment1,form=form, author=rent_author)
