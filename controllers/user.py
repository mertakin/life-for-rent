def login():
   username = request.vars.username
   password = request.vars.password
   enc_password = computeMD5hash(password)
   users = db((db.users.username == username) & 
              (db.users.password == enc_password)).select()
   print len(users)           
   if len(users) > 0:
      session.user = users[0]
   else:
      session.message = "xd"              
   redirect(request.env.http_referer)   
   # success = len(users) > 0            
   return len(users)

def logout():
   session.user = None
   redirect(request.env.http_referer)

def register():
   errors = {}
   values = {}
   errors["username"] = values["username"] = ""
   errors["pw1"] = ""
   errors["pw2"] = ""  
   values["first_name"] = "" 
   values["second_name"] = "" 
   values["email"] = ""
   errors["email"] = ""
    
   if request.vars:  # form is posted so start validating
      print request.vars
      username = values["username"] = request.vars.username
      email = values["email"] = request.vars.email
      first_name = values["first_name"] = request.vars.first_name
      second_name = values["second_name"] = request.vars.second_name
      pw1 = request.vars.pw1
      pw2 = request.vars.pw2
      if (username == ""):
         errors["username"] = "Can't be left empty"
      else:
         users = db().select(db.users.username)
         for user in users:
            if user.username == username:
                errors["username"] = "Sorry, that username is taken."  
      if (pw1 == ""):
         errors["pw1"] = "Can't be left empty"
      if (pw2 == ""):
         errors["pw2"] = "Can't be left empty"
      elif (pw1 != pw2):
         errors["pw2"] = "Passwords don't match"         
            
      validated = True
      for message in errors.values():
         if message != "":
            validated = False               
      print "Validated = " + str(validated)
      if validated:  # no errors found (form is validated) so we can process form data
         id = db.users.insert(username=username, 
                         password=computeMD5hash(pw1), email=email,
                          second_name=second_name,  first_name=first_name)      
         redirect(request.env.http_referer)                
   return dict(errors = errors, values=values)
