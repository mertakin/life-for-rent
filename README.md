# Life for Rent

### Summary

Life for Rent is a web application that allows users to rent and share their review with others.

### Motivation

The purpose of the project is to make a dynamic and interactive web platform that provides to rent everything that people would like.

Thanks to this platform, where all kinds of objects and quantities can be rented, it is aimed to change consumption habits and use the resources in the most efficient way.

### Tools

[Web2py](http://www.web2py.com/) 

[Limitless-Responsive Web Application Kit](http://demo.interface.club/limitless/demo/Template/layout_1/LTR/default/full/index.html)

## Demonstration

[Live demo is available on pythonanywhere](https://lifeforrent.pythonanywhere.com/WEB/default/index)

##### Login as admin

user name: mertakin

password: mertakin

![](snapshot.png)